﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GasolineraTrabajoFV
{
    class Program
    {
        static void Main(string[] args)
        {
            Facturacion factura = new Facturacion();//creamos el objeto de la clase facturacion

            //usamos los metodos a traves del objeto creado
            factura.Datos();
            factura.Subtotal();
            factura.CalculaIva();
            factura.Total();
            factura.Mostrar();
        }
    }
}

