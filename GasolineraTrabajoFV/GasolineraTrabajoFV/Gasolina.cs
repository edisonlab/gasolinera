﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GasolineraTrabajoFV
{
    class Gasolina
    {
        //caracteristicas de la gasolina
        private String tipoGasolina;
        private int cantidadGalones;
        private static double precioExtra = 1.50;
        private static double precioSuper = 2.00;

        public Gasolina()
        {
            tipoGasolina = " ";
            cantidadGalones = 0;
        }

        public String TipoGasolina
        {
            get
            {
                return tipoGasolina;
            }
            set
            {
                tipoGasolina = value;
            }
        }

        public int CantidadGalones
        {
            get
            {
                return cantidadGalones;
            }
            set
            {
                cantidadGalones = value;
            }
        }
        public double PrecioExtra
        {
            get
            {
                return precioExtra;
            }
        }
        public double PrecioSuper
        {
            get
            {
                return precioSuper;
            }
        }
    }
}

